#!/usr/bin/env nextflow

process yolotrim {
// Runs yolotrim
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - FASTQ Prefix
//     val(dataset) - Dataset
//     path(fq) - FASTQ
//   val parstr - Parameter String
//
// output:
//   tuple => emit: procd_fqs
//     val(pat_name) - Patient Name
//     val(run) - FASTQ Prefix
//     val(dataset) - Dataset
//     path("${dataset}-${pat_name}-${run}*.trimmed.f*q.gz") - Trimmed FASTQ 1

// require:
//   FQS
//   params.yolotrim$yolotrim_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'yolotrim_container'
  label 'yolotrim'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/yolotrim"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq)

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}-${pat_name}-${run}*.trimmed.f*q.gz"), emit: procd_fqs

  script:
  """
  yolotrim \
  --input ${fq} \
  --output ${dataset}-${pat_name}-${run}.trimmed.fq.gz  
  """
}
